
# Graceful Karaf Restart


Create a Chef repo symlink:
```
ln -s /root/sbd/cumulocity-chef/.chef/ .chef
```

Set cronjob to run at desired interval. For example:
```
# Daily Karaf Restart at 23:30 UTC (19:30 EST)
30 23 * * * /root/restart_core_services/restart.sh
```


Cron execution logs can be found here:
```
/var/spool/mail/root
```
