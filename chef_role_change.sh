#!/bin/bash
source /root/restart_core_services/utils.sh
source /root/restart_core_services/config.ini

edit_chef_role() {
    local action="$1"
    local node="$2"
    local role="$3"

    RESULTS=$(cd "$KNIFE_WORKDIR"; "$_knife_" node run_list "$action" "$node" "role[$role]")
    enter_to_log "$RESULTS"
}


chef_client() {
    local RESULTS
    declare -A lbs_ips

    # Create Associative Array with the lb nodes
    while read -r key value; do
        lbs_ips["$key"]="$value"
    done < <(puts_nodes_hostname_ip "lb")

    for key in "${!lbs_ips[@]}"; do
        RESULTS=$(ssh -i "$nodes_ssh_keys" -o "StrictHostKeyChecking no" -o "ConnectTimeout=5" "centos@${lbs_ips[${key}]}" 'sudo chef-client')
        enter_to_log "$key - $RESULTS"
    done
}
