#!/bin/bash

log_file='/var/log/cumulocity/restart_core_service.log'

# Create log file if it does not exist
if [[ ! -f $log_file ]]; then
	touch "$log_file"
fi

echo -e "\nSTOP----------------------------------------------------" >> "$log_file"
date >> "$log_file"
echo " - service cumulocity-core-karaf stop - $HOSTNAME" >> "$log_file"
service cumulocity-core-karaf stop >> "$log_file"
sleep 5 >> "$log_file" # Wait for Karaf to stop

timer=5
while [[ $(ps aux | grep karaf | grep -v grep) ]]; do
	sleep 5 >> "$log_file"
	timer=$((timer+5))

	PIDS_TO_KILL="$(ps aux | grep karaf | grep -v grep | awk '{print $2}')"
	if [[ $PIDS_TO_KILL ]]; then
		if [[ $timer -gt 60 ]] && [[ $timer -lt 70 ]]; then
			kill "$PIDS_TO_KILL" >> "$log_file"
			break
		fi

		if [[ $timer -gt 70 ]]; then
			kill -9 "$PIDS_TO_KILL" >> "$log_file"
			break
		fi
	fi

done

# Start Karaf
echo -e "\n--------------------------START-------------------------" >> "$log_file"
date >> "$log_file"
echo " - service cumulocity-core-karaf start - $HOSTNAME" >> "$log_file"
service cumulocity-core-karaf start >> "$log_file"
echo -e "\n--------------------------------------------------STATUS" >> "$log_file"
echo " - service cumulocity-core-karaf status - $HOSTNAME" >> "$log_file"
service cumulocity-core-karaf status >> "$log_file"
echo "Total time $timer seconds" >> "$log_file"
echo "-----------------------------------------------------END" >> "$log_file"
echo ""
