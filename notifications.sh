#!/bin/bash
source /root/restart_core_services/config.ini

email (){
    local email_body="$1"
    local email_addres="$2"
    if [[ -f $email_body ]]; then
        openssl s_client -crlf -quiet -starttls smtp -connect email-smtp.us-east-1.amazonaws.com:587 < $email_body;
    else
        enter_to_log "$log_file" "Body input file $email_body for email not found.";
        exit 1;
    fi
}

pushover () {
    local TITLE="${1}"
    local MESSAGE="${2}"

    curl \
    -F "token=${API_KEY}" \
    -F "user=${USER_KEY}" \
    -F "device=${DEVICE}" \
    -F "title=${TITLE}" \
    -F "message=${MESSAGE}" \
    "${PUSHOVERURL}" > /dev/null 2>&1
}
