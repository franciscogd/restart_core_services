#!/bin/bash

source /root/restart_core_services/chef_role_change.sh
source /root/restart_core_services/notifications.sh
source /root/restart_core_services/utils.sh
source /root/restart_core_services/config.ini

main() {
    cd "$WORKDIR"
    # Check for root user
    check_root
    local RESULTS
    declare -A core_ips

    # Create Associative Array with the core nodes
    while read -r key value; do
        core_ips["$key"]="$value"
    done < <(puts_nodes_hostname_ip "core")

    export editChefRoleRemoveException=101
    export chefClientException=102
    export scpException=103
    export karafRestartException=104
    export editChefRoleAddException=105
    export emailException=106
    ERRORs=0

    for key in "${!core_ips[@]}"; do
        try
        (
              ## Remove role[cumulocity-mn-active-core] in Chef
              RESULTS=$(edit_chef_role "remove" "$key" "cumulocity-mn-active-core") || throw $editChefRoleRemoveException

              # Update chef roles in LBs
              RESULTS=$(chef_client) || throw $chefClientException

              # SCP restart script to core
              RESULTS=$(scp -i "$nodes_ssh_keys" "karaf_restart.sh" "centos@${core_ips[${key}]}:$KARAF_WORKDIR") || throw $scpException

              # Restart Karaf in core
              RESULTS=$(
              ssh -i "$nodes_ssh_keys" -o "StrictHostKeyChecking no" -o "ConnectTimeout=5" "centos@${core_ips[${key}]}" <<EOF
                  sudo su -c "bash $KARAF_WORKDIR/karaf_restart.sh"
EOF
) || throw $karafRestartException

              ## Add role[cumulocity-mn-active-core] in Chef
              RESULTS=$(edit_chef_role "add" "$key" "cumulocity-mn-active-core") || throw $editChefRoleAddException

              # Update chef roles in LBs
              RESULTS=$(chef_client) || throw $chefClientException

          )
          catch || {
              ERRORs=1
              case $ex_code in
                    $editChefRoleRemoveException)
                      enter_to_log "$key - editChefRoleRemoveException - $RESULTS"
                      pushover "Weekly Karaf Restart - FAILURE - $key" "$RESULTS"
                      email "$email_body_failure" "$email_addresses"
                    ;;
                    $chefClientException)
                      enter_to_log "$key - chefClientException - $RESULTS"
                      pushover "Weekly Karaf Restart - FAILURE - $key" "$RESULTS"
                      email "$email_body_failure" "$email_addresses"
                    ;;
                    $scpException)
                      enter_to_log "$key - scpException - $RESULTS"
                      pushover "Weekly Karaf Restart - FAILURE - $key" "$RESULTS"
                      email "$email_body_failure" "$email_addresses"
                    ;;
                    $karafRestartException)
                      enter_to_log "$key - karafRestartException - $RESULTS"
                      pushover "Weekly Karaf Restart - FAILURE - $key" "$RESULTS"
                      email "$email_body_failure" "$email_addresses"
                    ;;
                    $editChefRoleAddException)
                      enter_to_log "$key - editChefRoleAddException - $RESULTS"
                      pushover "Weekly Karaf Restart - FAILURE - $key" "$RESULTS"
                      email "$email_body_failure" "$email_addresses"
                    ;;
                    $emailException)
                      enter_to_log "$key - emailException - $RESULTS"
                      pushover "Weekly Karaf Restart - FAILURE - $key" "$RESULTS"
                    ;;
                    *)
                      enter_to_log "$key - An unexpected exception was thrown - $RESULTS"
                      pushover "Weekly Karaf Restart - FAILURE - $key" "$RESULTS"
                      email "$email_body_failure" "$email_addresses"
                      throw $ex_code
                    ;;
            esac
          }
    done
    # Notify Successful Karaf restart
    if [[ $ERRORs -eq 0  ]]; then
        email "$email_body_success" "$email_addresses"
    fi
}

throwErrors #set -x   # activate debugging from here
main "$@"
ignoreErrors #set +x   # stop debugging from here
