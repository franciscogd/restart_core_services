#!/bin/bash
source /root/restart_core_services/config.ini

check_root(){
    if [[ $EUID -ne 0 ]]; then
         echo "This script must be run as root"
         exit 1
    fi
}


enter_to_log() {
    # Create log file if it does not exist
    if [[ ! -f $log_file ]]; then
    	 touch "$log_file"
    fi

    echo -e "------------------------------------ `date '+%Y-%m-%d %H:%M:%S:%N'` ------------------------------------" >> "$log_file"
    echo -e "$1" >> "$log_file"
}

puts_nodes_hostname_ip(){
    filter="$1"
    cd "$KNIFE_WORKDIR"; "$_knife_" exec -E 'nodes.all.each {|n| print(n["hostname"], " ", n["ipaddress"], "\n") }' | grep "$filter"
}


try() {
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

throw() {
    exit $1
}

catch() {
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}

throwErrors() {
    set -e
}

ignoreErrors() {
    set +e
}
